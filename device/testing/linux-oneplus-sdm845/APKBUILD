# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="oneplus-sdm845"
pkgname=linux-$_flavor
pkgver=5.9
pkgrel=0
pkgdesc="Kernel fork for OnePlus SDM845 devices, close to mainline"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/sdm845-linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="bison findutils flex installkernel openssl-dev perl"

_config="config-$_flavor.$arch"
_commit="e44ef9ac67ee277e2d480a6f7247224c667a56dd"

# Source
source="
	sdm845-linux-$_commit.tar.gz::https://gitlab.com/sdm845-mainline/sdm845-linux/-/archive/$_commit/sdm845-linux-$_commit-sdm845.tar.gz
	$_config
"
builddir="$srcdir/sdm845-linux-$_commit"

prepare() {
	default_prepare
	mkdir -p "$srcdir"/build
	cp -v "$srcdir"/$_config "$srcdir"/build/.config
	make -C "$builddir" O="$srcdir"/build ARCH="$_carch" \
		olddefconfig
}

build() {
	cd "$srcdir"/build
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	cd "$srcdir/build/arch/$_carch/boot"

	install -D "$srcdir/build/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	cd "$srcdir"/build
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

sha512sums="dabff5214c3ca127943e8dfd4681d589b2910d78e4758be912448f65d8ade9dd59d7771fad4623d1a7050dc187b39c24b60c0ed3bbeae55021770449f349e299  sdm845-linux-e44ef9ac67ee277e2d480a6f7247224c667a56dd.tar.gz
987eafaec32274b0a1896d20daaecf699f86dafbbf2c91d968e9fb3a5edafb2977aef1608a36cc171abdac8a7daa0d255aa61aad633e28e65ebbebf5b1d4002c  config-oneplus-sdm845.aarch64"
